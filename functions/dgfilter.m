function [signal_smoothed, winlist] = dgfilter(  signal, sigma )
% Scale-normalized differential Guassian filter
% Input: 
%   signalは行方向の時系列データ
%   sigmaは利用するsigma値のリスト
% 説明: 
%   スケールの空間で極値が不変になるようなscale-normalized 微分フィルタ

signal_smoothed = zeros( length(sigma), size(signal,2));
% gaussian' を畳み込む
wsize0 = 10;   % 窓の幅。sigmaの比
winlist = cell(length(sigma), 1);
for s = 1:length(sigma)
    s2val = sigma(s)*sigma(s);
    winsize = ceil(wsize0 * sigma(s)) + 1;
    m = floor(winsize/2)+1; % 
    x = (1:winsize) - repmat(m,1,winsize); % windowの中心がx=0になるように(wsize偶数なら一つ後ろ)
    %window = sval * ( (-x/(sqrt(2*pi)*sval*sval*sval)) .* exp( - (x.^2)/(2*sval*sval)) ); % 通常のガウシアンの一次微分にsigmaをかける(一次微分なのでこれでscale-normalizedになる)
    window = (-x/(sqrt(2*pi)*s2val)) .* exp( - (x.^2)/(2*s2val)); % 通常のガウシアンの一次微分にsigmaをかける(一次微分なのでこれでscale-normalizedになる)
    winlist{s} = window;
    %figure(99),subplot(211), plot(window)
    signal_smoothed(s,:) = convn(signal, window, 'same');
    %subplot(212), plot(signal_smoothed(s,:)), drawnow;
end

end
