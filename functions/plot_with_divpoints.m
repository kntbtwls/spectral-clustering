function [ret1, ret2, ret3] = plot_with_divpoints( Iset, varargin )
%UNTITLED この関数の概要をここに記述
%   詳細説明をここに記述
if(islogical(varargin{1}) && varargin{1})
    [ret1, ret2, ret3] = plotyy(varargin{2:end});
else
    ret1 = plot(varargin{:});
    ret2 = [];
    ret3 = [];
end
ylabel('signal')
ylm = get(gca, 'YLim');
for k=2:size(Iset,1)
    line([Iset(k,1),Iset(k,1)], ylm, 'Color', 'k');
end


end

