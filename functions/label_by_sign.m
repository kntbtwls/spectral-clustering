function [ scLDS_idx ] = label_by_sign( EVECgraph, scLDS_idx, ioi )

vec = EVECgraph(:,2);

for i = 1:size(vec,1)
    if vec(i) >= 0
        scLDS_idx(ioi(i),1) = scLDS_idx(ioi(i),1) .* 10 + 1;
    else
        scLDS_idx(ioi(i),1) = scLDS_idx(ioi(i),1) .* 10 + 2;
    end
end

end
