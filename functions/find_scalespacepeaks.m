function [ C1s_maxpeak, C1s_minpeak, C1s_peakmat ] = find_scalespacepeaks( C1s, MinScale, MinPeakHeight, MinDiffFromNeighbor, create_peakmat )
%find_scalespacepeaks scalespaceにて8近傍での極大値および極小値を見つける
%   C1s: 入力 (行: スケール, 列: 時間)
%   MinScale: 探索を始める最小のスケール

if(create_peakmat)
    C1s_peakmat = C1s;
else
    C1s_peakmat = [];
end
C1s_maxpeak = [];
C1s_minpeak = [];
maxscale = size(C1s,1);
%sizeofmat = size(C1s);

%for s=MinScale:maxscale
for s=MinScale:maxscale-1
    for t=2:size(C1s,2)-1
        % 8近傍 (neighbor8の配列の作り方，もう少しなんとかならないか)
        neighbors8 = C1s(s, [t-1,t+1]); 
        if(s>1), neighbors8 = [neighbors8, C1s(s-1, t-1:t+1)]; end
        if(s<maxscale), neighbors8 = [neighbors8, C1s(s+1, t-1:t+1)]; end
        %neighbors8 = [C1s(s-1, t-1:t+1), C1s(s, [t-1,t+1]), C1s(s+1, t-1:t+1)]; 
        centerval = C1s(s,t); % 中心の値
        abscenterval = abs(centerval); 
        if(abscenterval < MinPeakHeight) % 指定した最小のピーク高より小さければスキップ
            continue
        end
        if(centerval > 0 && centerval >= max(max( neighbors8 )) + MinDiffFromNeighbor) % 極大(傾き正
            C1s_maxpeak = [C1s_maxpeak; [s,t]];
            if(create_peakmat), C1s_peakmat(s,t) = inf; end
        end
        if(centerval < 0 && centerval <= min(min( neighbors8 )) - MinDiffFromNeighbor) % 極小
            C1s_minpeak = [C1s_minpeak; [s,t]];
            if(create_peakmat), C1s_peakmat(s,t) = -inf; end
        end            
    end
end

end

