function sqerrors2d = calc_all_sqerrors(X, Iset, lmax, LDSalgo, LDSorder)
%CALC_ALL_ERRORS
% あらかじめ区間を結合したときの再現誤差を計算しておく
% 入力:   X (dim x T) 信号
% 入力:   Iset (K x 2) 区間系列
% 入力:   lmax 区間の最大結合数
% 出力:   errors2d(dur, begI)
% 第一要素が連結区間数 (1以上, lmax以下)
% 第二要素が開始区間index


debug = false; % Debug用表示

K = size(Iset,1);
len = size(X,2);
sqerrors2d = zeros( min(lmax, K), K); % initialization


fprintf('calc_all_errors:\n');
for bk=1:K
    if(mod(bk,10)==0),fprintf('.');end
    ek_max = min(bk+lmax-1, K);
    for ek = bk:ek_max
        dur = ek - bk + 1;
        
        %{
        X0 = X(:, Iset(bk, 1):Iset(ek, 2)-1);
        X1 = X(:, Iset(bk, 1)+1:Iset(ek, 2));
        m0 = mean(X0,2);
        m1 = mean(X1,2);
        X0c = bsxfun(@minus, X0, m0);
        A = X1*pinv(X0c);
        b = m1 - A*m0;        
        err = norm(A*X0+repmat(b,1,size(X0,2))-X1, 'fro');
        sqerrors2d(dur, bk) = err*err;
        %}
        Xtmp{1} = X;
        Isettmp{1} = [Iset(bk, 1), Iset(ek, 2)];
        % 
    	[A, b, Q, xinit, tmpalpha, tmpbeta, tmpgamma, Acov, Acovinv, Err] = identify_system(Xtmp, Isettmp, LDSalgo, true, true, 1, LDSorder);
        sqerrors2d(dur, bk) = sum(sum(Err.^2));
        %sqerrors2d(dur, bk) = simpleAR(X, Iset(bk,1), Iset(ek,2));
        if(debug), fprintf('t = [%d, %d], (dur, bk, ek) =  (%d, %d, %d), err = %f\n', Iset(bk, 1), Iset(ek, 2), dur, bk, ek, sqerrors2d(dur,bk)); end

    end
end
fprintf(' done.\n')

end
