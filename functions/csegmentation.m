function viterbiIset = csegmentation(signal, param)
normseq = var_norm(signal);
sigma = 1:100;
C0 = scalespace(normseq, sigma);
C = diff(C0,2,2);
vidlen = length(normseq);
lmax = zeros(size(C,1), size(C,2));

for i = 1:size(C,1)
    lmax(i,:) = zerocross(C(i,:));
end

C_old = C;
lmax_old = lmax;
% ゼロクロスが一度も出てこないスケールを削除
C=C(any(lmax'),:);
lmax=lmax(any(lmax'),:);

lmax_old2 = lmax;
% 系列の始めの方と終わりの方の変曲点は使わない
EDGE = 9;
lmax(:,[1:1+EDGE,end-EDGE:end]) = 0;

%% 変曲点のトレース
startlevel = 20;
endlevel = 1;
lmaxptr = tracking(startlevel, endlevel, lmax);
lmaxptr = lmaxptr';

Iset = [1, lmaxptr'; lmaxptr'-1, size(lmax,2)]'; % 区間系列


% 短すぎる区間は連結
modifiedIset = concatenate_short_intervals(Iset, param.lmin);

% 長さに関するViterbi
[viterbiIset, totalerror, intervalerrors] = viterbi_concatenate_intervals(modifiedIset, signal, param.divCost, param.max_interval_cat, param.LDSalgo, param.LDSorder); 

