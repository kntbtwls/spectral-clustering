function [ scLDS_idx ] = spectral_clustering( Wgraph, Dist, scLDS_idx )
    % ラベル毎に処理
    label_num = unique(scLDS_idx');
    for i = label_num
        ioi = find(scLDS_idx' == i);     % あるラベルの付いている区間番号だけ取り出す
        if(size(ioi,2) < 2)
            continue
        end

        Dist_ioi = Dist(ioi, ioi');
        Wgraph_ioi = Wgraph(ioi, ioi');     % 対応する部分行列だけ取り出す

        Dgraph = diag(sum(Wgraph_ioi,2));
        %Lgraph = eye(size(Dist_ioi,1)) - (Dgraph^-1)*Wgraph_ioi; % normalization rw
        Lgraph = eye(size(Dist_ioi,1)) - (Dgraph^-0.5)*Wgraph_ioi*(Dgraph^-0.5); %normalization sym
        [EVECgraph, EVALgraph] = eig(Lgraph);

         if(EVALgraph(2,2) > 1.0)
             fprintf('eval: %f\n', EVALgraph(2,2));
             continue
         end

        scLDS_idx = label_by_sign(EVECgraph, scLDS_idx, ioi);


    end

end
