function dist = calc_distance(ci, cj, type_of_distance, param, Xs)
%%
%  (距離の定義を少し追加)
% KLなどは距離の二乗を返す
%     (TODO: set lds.Q before calling this function)

if(nargin < 5)
    Xs = [];
    if(type_of_distance==1 || type_of_distance==12)
        error('In function calc_distance: set Xs for KL divergence');
    end
end

if(nargin < 4)
    param = struct(); % empty struct
end

if(isfield(param, 'manualQdiag'))
    manualQdiag = param.manualQdiag;
elseif(type_of_distance==1 || type_of_distance==12)
    error('In function calc_distance: set param.manualQdiag');
end

if(isfield(param, 'centerize'))
    centerize = param.centerize;
end

if(isfield(param, 'mu'))
    mu = param.mu;
end

%{
if(isfield(param, 'sigma2'))
    sigma2 = param.sigma2;
elseif(type_of_distance==13 || type_of_distance==43)
    error('In function calc_distance: set param.sigma');
end
%}

n = size(ci.A, 1);
order = size(ci.A, 2)/n;
if type_of_distance==0
    % MartinDistance
    if isfield(param, 'centerize') && centerize
        dist = MartinDistance([ci.A ci.b; zeros(1, n) 1], [eye(n) zeros(n, 1)],...
                              [cj.A cj.b; zeros(1, n) 1], [eye(n) zeros(n, 1)] );
    else
        dist = MartinDistance(ci.A, [], cj.A, [] );
    end

elseif type_of_distance==1 || type_of_distance==12 %|| type_of_distance==13
    % Approximated KLDist (of sample data distributions)
    if (manualQdiag < 0)
        dQi = diag(ci.Q);   dQj = diag(cj.Q);
    else
        dQi = manualQdiag*ones(n,1);  dQj = manualQdiag*ones(n,1);
    end
    KLdisttype = mod(type_of_distance, 10);
    dist = KLDistance(Xs, ci.Iset, Xs, cj.Iset, ci.A, ci.b, dQi, cj.A, cj.b, dQj, KLdisttype);
    

elseif type_of_distance==2
    % Bayes factor by using predictive distribution
    dist = BFDistance(Xs, ci, cj);

elseif type_of_distance==3
    % Bayes: KLdiv of two parameter distributions
    dist = KLParamDistance(ci, cj);
elseif type_of_distance==4 
    % Direct comparison of parameters
    dist = mu.A * norm(ci.A - cj.A, 'fro')^2 + mu.b * norm(ci.b - cj.b)^2 + mu.xinit * norm(ci.xinit - cj.xinit)^2;
elseif type_of_distance==42 %|| type_of_distance==43
    % Direct comparison of parameters
    %tic
    %bias_i = (eye(n,n) - ci.A * kron(eye(n,n), ones(order, 1)) ) \ ci.b;
    %bias_j = (eye(n,n) - cj.A * kron(eye(n,n), ones(order, 1)) ) \ cj.b;
    % below is faster
    tmpA_i = eye(n,n);
    tmpA_j = eye(n,n);
    for i=1:order
        tmpA_i = tmpA_i - ci.A(:,((i-1)*n+1):(i*n));
        tmpA_j = tmpA_j - cj.A(:,((i-1)*n+1):(i*n));
    end
    bias_i = tmpA_i \ ci.b;
    bias_j = tmpA_j \ cj.b;
    %toc
    dist = mu.A * norm(ci.A - cj.A, 'fro')^2 + mu.b * norm(bias_i - bias_j)^2 + mu.xinit * norm(ci.xinit - cj.xinit)^2;
end

%{
if(type_of_distance==13 || type_of_distance==43)
    dist = 1-exp(-dist/sigma2);
end
%}

end
