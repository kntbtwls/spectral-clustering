function [ labelseq ] = interval2labelseq( Iset )
%interval2labelseq ラベル付き区間系列 (Ik (k=1,...,Nk) を，時間方向に展開してラベル系列を作る
%   Iset (区間数 x 3 の行列) : 各行は 始点，終点，ラベル（ただし区間は [始点, 終点]
%   と考え，区間同士はオーバラップ無しとする）
%  labelseq: 列ベクトルのラベル系列

assert( size(Iset,2) == 3, '@interval2labelseq: Row of Iset should be [beg, end, label]')

% 念のためソート(始点)
[dummy, newindex] = sort(Iset(:,1));
newIset = Iset(newindex, :);

T = Iset(end,2);
labelseq = zeros(T,1);
for k=1:size(newIset,1)
    labelseq( newIset(k,1):newIset(k,2) ) = newIset(k,3);
end

end

