function point2label = relabel_by_idx( assignedlabel )
    point2label = zeros(size(assignedlabel,1),1);
    idx_num = unique(assignedlabel);

    for i=1:size(idx_num, 1)
        point2label(find(assignedlabel == idx_num(i,1)),1) = i;
    end
