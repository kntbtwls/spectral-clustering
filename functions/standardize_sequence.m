function [ Xret ] = standardize_sequence( X )
%standardize_sequence 標準化 (Xの各行が平均0, 標準偏差1になるように)
%   Input X: dim x T
mean_x = mean(X,2);
std_x = std(X,[], 2);
Xret = (X - repmat(mean_x, 1, size(X,2) ))./repmat(std_x, 1, size(X,2) );
end

