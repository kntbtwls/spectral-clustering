function distance = KLDistance(X1, Is1, X2, Is2, A1, b1, Q1diag, A2, b2, Q2diag, KLdisttype)
%% KLDISTANCE
% do not consider initial states; only check dynamics (A,b)

if(nargin < 11)
    KLdisttype = 1;
end

    [n n_ord] = size(A1);
    % equals to size(A2)
    if n_ord ~= size(A2,2)
        fprintf('############### different order!!! ################')
        distance = 0;
    end
    order = n_ord / n;

    loglike11 = 0;
    loglike12 = 0;
    totallen1 = 0;
    for s=1:size(Is1,2)
        for k=1:size(Is1{s}, 1)
            b = Is1{s}(k, 1);
            e = Is1{s}(k, 2);
            X1past = [];
            for p=0:order-1
                X1past = [ X1past; X1{s}(:, b+p:e-(order-p) ) ];
            end
            len = size(X1past,2);
            %   loglike11 = loglike11 + calcloglike( X1(:, b+1:e) - A1*X1(:, b:e-1) - b1*ones(1, e-b),  Q1diag );
            %   loglike12 = loglike12 + calcloglike( X1(:, b+1:e) - A2*X1(:, b:e-1) - b2*ones(1, e-b),  Q2diag );
            if(KLdisttype==2)
                loglike11 = loglike11 - norm( diag(1./sqrt(Q1diag)) * ( X1{s}(:, b+order:e) - generate_sequence(X1past(:,1), A1, b1, len ) ),  'fro' )^2;
                loglike12 = loglike12 - norm( diag(1./sqrt(Q2diag)) * ( X1{s}(:, b+order:e) - generate_sequence(X1past(:,1), A2, b2, len ) ),  'fro' )^2;                
            else % default
                loglike11 = loglike11 - norm( diag(1./sqrt(Q1diag)) * ( X1{s}(:, b+order:e) - A1*X1past - b1*ones(1, len ) ),  'fro' )^2;
                %    try
                loglike12 = loglike12 - norm( diag(1./sqrt(Q2diag)) * ( X1{s}(:, b+order:e) - A2*X1past - b2*ones(1, len ) ),  'fro' )^2;
            end
            %     catch
            %       size( X1{s}(:, b+order:e) )
            %       size( A2*X1past)
            %       size(b2*ones(1, len ) )
            %     end
            totallen1 = totallen1 + len;
        end
    end

    loglike21 = 0;
    loglike22 = 0;
    totallen2 = 0;
    for s=1:size(Is2,2)
        for k=1:size(Is2{s}, 1)
            b = Is2{s}(k, 1);
            e = Is2{s}(k, 2);
            X2past = [];
            for p=0:order-1
                X2past = [ X2past; X2{s}(:, b+p:e-(order-p) ) ];
            end
            len = size(X2past,2);
            %   loglike21 = loglike21 + calcloglike( X2(:, b+1:e) - A1*X2(:, b:e-1) - b1*ones(1, e-b),  Q1diag );
            %   loglike22 = loglike22 + calcloglike( X2(:, b+1:e) - A2*X2(:, b:e-1) - b2*ones(1, e-b),  Q2diag );
            if(KLdisttype==2)
                loglike21 = loglike21 - norm( diag(1./sqrt(Q1diag)) * ( X2{s}(:, b+order:e) - generate_sequence(X2past(:,1), A1, b1, len ) ),  'fro' )^2;
                loglike22 = loglike22 - norm( diag(1./sqrt(Q2diag)) * ( X2{s}(:, b+order:e) - generate_sequence(X2past(:,1), A2, b2, len ) ),  'fro' )^2;                
            else % default
                loglike21 = loglike21 - norm( diag(1./sqrt(Q1diag)) * ( X2{s}(:, b+order:e) - A1*X2past - b1*ones(1, len) ),  'fro' )^2;
                %    try
                loglike22 = loglike22 - norm( diag(1./sqrt(Q2diag)) * ( X2{s}(:, b+order:e) - A2*X2past - b2*ones(1, len) ),  'fro' )^2;
            end
            %     catch
            %       size( X2{s}(:, b+order:e) )
            %       size( A2*X2past)
            %       size(b2*ones(1, len ) )
            %     end
            totallen2 = totallen2 + len;
        end
    end
    distance = max(0,  ( (loglike11 - loglike12)/totallen1 + (loglike22 - loglike21)/totallen2 ) / 4.0 );
    %fprintf('distance= %f | loglike11= %f, loglike12= %f, loglike21= %f, loglike22= %f\n', distance, loglike11, loglike12, loglike21, loglike22);

end

%% NOTE!!  We do not have to calculate 'real' loglike.
% Most of the terms in the loglikelihood calculation are cancelled out at the
% distance above
%% Note1
% loglike11 - loglike12
% = (-sum_d11 + sum_d12)/totallen1 + totallen1*dim*logstatesize/totallen1
%     - totallen1*dim*logstatesize/totallen1
% = (-sum_d11 + sum_d12)/totallen1
% Therefore, we get
%   distance = (  (-sum_d11+sum_d12)/totallen1 + (-sum_d22+sum_d21)/totallen2  ) / 2.0;
%
% So, we can omit the term:
%     len*dim*logstatesize
% at loglike calculation
%% Note 2
%  0.5 * dim * log(2*pi) in likelihood calculation is also cancelled out:
%   d(t) = sumlogvar + scalederr(t)/2 = 0.5 * (sumlogvar + sum( Err(:,t).^2 ./ Qdiag ) )
% Adding for i=1:len, we get
%   sum_t d(t) = 0.5 * (len*sumlogvar + norm(diag(1./sqrt(Qdiag)) * Err, 'fro')^2 )
%   sum_d = sum_I sum_t d
%         = 0.5 * (totallen*sumlogvar + sum_I norm(diag(1./sqrt(Qdiag)) * Err, 'fro')^2 )
%% Note 3
% Using Note2, the first term of
%    (-sum_d11+sum_d12)/totallen1
% in Note1 becomes:
%   -0.5 * (totallen1*sumlogvar1 - totallen1*sumlogvar2)/totallen1
%  = -0.5 * (sumlogvar1 - sumlogvar2)  --(*1)
% The first term of
%     (-sum_d22+sum_d21)/totallen2
% becomes:
%   -0.5 * (totallen2*sumlogvar2 - totallen2*sumlogvar1)/totallen2
%  = -0.5 * (sumlogvar2 - sumlogvar1)  --(*2)
% Thus,
%   (*1) + (*2) = 0
% and we need to calculate only the errors scaled by variances.

% function loglike = calcloglike(Err, Qdiag)
% %% CALCLOGLIKE
% %
% [dim, len] = size(Err);
% logstatesize = sqrt(2*pi*mean(Qdiag));  % see Note1
%
% sumlogvar = sum( log(Qdiag) );
% d = 0;
% for i=1:len
%   sumerr = sum( Err(:,i).^2 ./ Qdiag);
%   d = d + 0.5 * (dim * log(2*pi) + sumlogvar + sumerr);
% end
% loglike = -d + len*dim*logstatesize;
%
% end
