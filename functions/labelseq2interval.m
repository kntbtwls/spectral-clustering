function [ labeledIset ] = labelseq2interval( labelseq )
%labelseq2interval この関数の概要をここに記述
%   詳細説明をここに記述
if( size(labelseq, 1) ==1 )
    labelseq = labelseq';
end
% 列ベクトルとして扱う

idx = find(diff(labelseq)~=0);

len = length(labelseq);
labeledIset = [ [1;idx+1], [idx;len], labelseq([idx;len]) ];

end

