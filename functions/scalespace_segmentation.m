function [ viterbiIset ] = scalespace_segmentation( X, param )
%scalespace_segmentation
%微分ガウスフィルタでスケールスペースを構築しピークを検出することで初期セグメンテーションを行い，その後LDSに近似できる区間を統合する
%  INPUT: 値は例
%{
% param.sigmalist はスケールスペースの構築に使う sigma のベクトル
%  もし渡さなければ自動計算
%
%
% --- Parameters for finding scalespace peaks
ssparam.MinPeakHeight = 0.02 * sqrt(dim); % ピークの絶対値条件
ssparam.MinDiffFromNeighbor = 0.00; % 周辺との差がこの値より大きい場合のみ検出
ssparam.MinScale = 2;

% --- Parameters for Viterbi Concatenate
ssparam.param.divCost = 0.03; % 分割に要するコスト この値が小さいほど分割が増える
ssparam.max_interval_cat = 40;
ssparam.lmin = LDSorder + 1;

pkthres = 0.004; %pkthres = 0.002; %## param ## 平らなピークを除外（ピークとその両隣との間には，最小振幅差としてpkthresが必要とする）

%}

if( ~isfield(param, 'sigmalist') ) % sigmalistが渡されていないならここで計算
    sigma0 = 1;
    maxscale = 10;
    k_for_sigma = 2^(1/2);
    param.sigmalist = sigma0 * k_for_sigma.^(0:maxscale-1); % sigma0 * kのべき乗 でsigmaのリストを作成
    fprintf('Use default sigmalist\n');
end

    %% 元の信号に対する scale-normalized differential Gaussian filtering
    normdiffseq = sqrt(sum((X(:, 2:end) - X(:, 1:end-1)).^2, 1)); % signal のノルムを計算
    %normdiffX = (normdiffX - mean(normdiffX))/std(normdiffX); % 標準化
    [C1s, dgwinlist] = dgfilter(normdiffseq, param.sigmalist);

    % scale-time 空間で8近傍での極大，極小検出
    [C1s_maxpeak, C1s_minpeak, C1s_peakmat] = find_scalespacepeaks(C1s, param.MinScale, param.MinPeakHeight, param.MinDiffFromNeighbor, true);

    figure('Name', 'Segmentation (by Scale-normalized differential Gaussian)', 'Position', [80,50,1600,800]);
    Nrows = 5;
    clim = [-0.2, 0.2]; % color lim
    subplot(Nrows, 1, 1), imagesc(C1s(1:maxscale,:), clim); %colorbar;
    subplot(Nrows, 1, 2), imagesc(C1s_peakmat(1:maxscale,:), clim); %colorbar;
    drawnow

    % TODO: 速度ノルムが小さすぎる場合は削除? -> したほうがいい

    % 極大は負の方向へ，極小は正の方向へたどって速度ノルムの勾配のゼロ交差を見つける
    %C1s_maxpeak_updated = C1s_maxpeak;
    %C1s_minpeak_updated = C1s_minpeak;
    for i=1:size(C1s_maxpeak, 1)
        s = C1s_maxpeak(i,1); % どのスケールでたどるか
        torig = C1s_maxpeak(i,2); % たどるスタート時刻
        peakval_search = C1s(s,torig) * param.peakratio_for_shift; % 正の値なはず
        for t=torig:-1:2
            if(C1s(s, t-1) < peakval_search), break; end % ゼロ交差があれば終了
        end
        %C1s_maxpeak_updated(i,2) = t;
        C1s_maxpeak(i,2) = t-1; % ここはtではなくt-1とすることで，極小をたどった点とマージさせる
    end
    for i=1:size(C1s_minpeak, 1)
        s = C1s_minpeak(i,1); % どのスケールでたどるか
        torig = C1s_minpeak(i,2); % たどるスタート時刻
        peakval_search = C1s(s,torig) * param.peakratio_for_shift;
        for t=torig:1:size(C1s,2)-1
            if(C1s(s, t) > peakval_search), break; end % ゼロ交差があれば終了
        end
        %C1s_minpeak_updated(i,2) = t;
       % fprintf('(i,s,t) = (%d, %d, %d)\n', i,s,t);
        C1s_minpeak(i,2) = t;
    end

    %% Scale-normalized differential Gaussian後のピーク検出結果
    trange= 1:length(normdiffseq); % 表示範囲
    subplot(Nrows, 1, 3), ax = plotyy(trange, normdiffseq(:,trange)', trange, X(:,trange)'); %set(ax(1),'YLabel', 'Diffnorm'), set(ax(2), 'YLabel', 'Signal');
     set(ax(1), 'XLim', [0,length(normdiffseq)]); set(ax(2), 'XLim', [0,length(normdiffseq)]);
    scalepospk_locs = C1s_maxpeak(:,2);
    scalenegpk_locs = C1s_minpeak(:,2);
    pospklocs = scalepospk_locs(scalepospk_locs >= trange(1) & scalepospk_locs <= trange(end)); % プロット範囲内だけ取り出す
    negpklocs = scalenegpk_locs(scalenegpk_locs >= trange(1) & scalenegpk_locs <= trange(end)); % プロット範囲内だけ取り出す
    hold on
    %plot(pospklocs, X(pospklocs), '^', 'Color', [0.7, 0.3, 0]), plot(negpklocs, X(negpklocs), 'v', 'Color', [0, 0.3, 0.7])
    plot(pospklocs, normdiffseq(pospklocs), '^', 'Color', [0.7, 0.3, 0]), plot(negpklocs, normdiffseq(negpklocs), 'v', 'Color', [0, 0.3, 0.7])
    hold off
    drawnow




    %% ビタビで隣接区間を連結し区間系列を生成
    if(param.use_origpeak)
        % 元信号のピーク検出---(ひとまず第一要素の系列のみ使用してピーク検出)
        s_signal = smooth(X(1,:), 8, 'moving'); % さらに平滑化しておく
        [origpospk_vals, origpospk_locs] = findpeaks(s_signal, 'Threshold', param.pkthres); %locはlocationの略
        [orignegpk_vals, orignegpk_locs] = findpeaks(-s_signal, 'Threshold', param.pkthres);
        divpts = unique(sort([origpospk_locs; orignegpk_locs;scalepospk_locs; scalenegpk_locs])); % 元信号のピークと scale-normalized differential gaussian でのピークを利用
    else
        divpts = unique(sort([scalepospk_locs; scalenegpk_locs])); % scale-normalized differential gaussian でのピークのみを利用
    end
    fprintf('Number of dividing points: %d\n', length(divpts));
    if(divpts(1)==1)
        segIset = [divpts'; divpts(2:end)'-1, size(C1s,2)]';
    else
        segIset = [1, divpts'; divpts'-1, size(C1s,2)]'; % 区間系列
    end

    % 短すぎる区間は連結
    modifiedIset = concatenate_short_intervals(segIset, param.lmin);

    % 長さに関するViterbi
    [viterbiIset, totalerror, intervalerrors] = viterbi_concatenate_intervals(modifiedIset, X, param.divCost, param.max_interval_cat, param.LDSalgo, param.LDSorder);


    % 結果表示 show dividing points by vertical lines
    subplot(Nrows, 1, 4)
    ax = plotyy_with_divpoints(segIset, trange, normdiffseq(:,trange)', trange, X(:,trange)');
     set(ax(1), 'XLim', [0,length(normdiffseq)]); set(ax(2), 'XLim', [0,length(normdiffseq)]);
    fprintf('Number of intervals (concatenated by Viterbi): %d\n', size(viterbiIset,1));
    subplot(Nrows, 1, 5)
    ax = plotyy_with_divpoints(viterbiIset, trange, normdiffseq(:,trange)', trange, X(:,trange)');
     set(ax(1), 'XLim', [0,length(normdiffseq)]); set(ax(2), 'XLim', [0,length(normdiffseq)]);


end
