function [ signal_smoothed ] = scalespace( signal, sigma )
% 時系列データを平滑化
% signalは行方向の時系列データ
% sigmaは利用するσの値の集合

% gaussianを畳み込む
signal_smoothed = zeros( length(sigma), size(signal,2));
wsize0 = 10;   % 窓の幅。sigmaの比
for s = 1:length(sigma)
    winsize = ceil(wsize0 * sigma(s)) + 1;
    window = fspecial('gaussian', [winsize,1], sigma(1,s));

    window = window / sum(window); % fspecial ではsum=1が保障されておりこれは不要

    signal_smoothed(s,:) = convn(signal, window', 'same');
end

end
