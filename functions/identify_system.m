function [Ahat, bhat, Qhat, xinithat, alpha, beta, gamma, Acov, Acovinv, Err] = identify_system(Xs, Iset, algo, centerize, skip_refinement, upper_bound, order)

    X1 = [];
    X2 = [];
    xinitlist = [];
    if isempty(order)
        order = 1;
    end
    %disp('Iset');size(Iset)
    for s=1:size(Iset,2) % for each sequence
                         %   fprintf('s: %d\n', s);
                         %   Xs{s}
        for k=1:size(Iset{s}, 1); % for each Interval
            b = Iset{s}(k,1);
            e = Iset{s}(k,2);
            X1tmp = [];
            for p=0:order-1
                X1tmp = [ X1tmp; Xs{s}(:, b+p:e-order+p ) ];
            end
            X1 = [X1 X1tmp];
            X2 = [X2 Xs{s}(:, b+order:e) ];
            xinitlist = [xinitlist X1tmp(:, 1)];
        end
    end
    [Ahat, Qhat, Err, bhat, alpha, beta, gamma, Acovinv] = learnAR(X1, X2, algo, centerize, skip_refinement, upper_bound);
    if(Acovinv~=0)
        %  Acov = inv(Acovinv);
        Acov = 1./diag(Acovinv);
    else
        Acov = 0;
    end
    xinithat = mean(xinitlist, 2); % initial state
                                   % size(X1)
                                   % size(X2)
                                   % size(xinithat)

end
