function [ landmarkseqSet, headseqSet ] = load_sequences( datadir_root, seqidlist )
%load_sequences
%   seqidlist で指定したIDの系列を読み込む
%   seqidlist は省略可能 (省略した場合はすべて読み込む)
%   現在5系列

seqlist = {'ukQ1', 'ukQ2', 'thQ2', 'thQ4', 'ynQ4'};

if(nargin < 2)
    seqidlist = 1:5 % 全て
end

seqidlist

nseq = length(seqlist);
landmarkseqSet = cell(1,nseq);
headseqSet = cell(1,nseq);

for sid=seqidlist
    %sid
    seqname = seqlist{sid};
    % ----------------------------- 読み込みここから ----------------------------------
    % load Zhang's tracking result
    %datadir_root = '../data/HmnkZhang20150427';
    matfname1 = [seqname, '_3DShape_faceTracker.mat']
    matfname2 = [seqname, '_globalParameter_faceTracker.mat']

    % loadしたときにいちいち変数名が変わるのでwhosで自動取得する
    varlist = whos('-file', [datadir_root, '/', matfname1]);
    tmp = load( [datadir_root, '/', matfname1] );
    XYZ = tmp.(varlist(1).name); % 文字列で変数名を指定するときは括弧で囲んで構造体配列から読めばよい

    T = size(XYZ,1);
    Npts = size(XYZ,2)/3;
    landmarkseqSet{sid} = reshape(XYZ, T, Npts, 3);
    %landmarkseq = permute(reshape(T, Npts, 3), [1 3 2]);
    %X3d = XYZ(:,1:66);
    %Y3d = XYZ(:,(66+1):66*2);
    %Z3d = XYZ(:,(66*2+1):66*3);
%[pitch,yaw,roll,scale,tx,ty]
    varlist = whos('-file', [datadir_root, '/', matfname2]);
    tmp = load( [datadir_root, '/', matfname2] );
    headseqSet{sid} = tmp.(varlist(1).name);

end
