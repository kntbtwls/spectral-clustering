function [ point2label, clustCent, gSizeSort ] = relabel_by_size( assignedlabel, gMean )
%relabel_by_size  ラベルを振りなおす (サイズの大きい順にラベルを振ることにする)
% サイズゼロのクラスタも取り除く
% クラスタ平均: Input: gMean -> Output: clustCent (行ベクトルがcentroidとする)
% クラスタラベル: Input: assignedlabel -> Output: point2label
% にアップデートされる
%  Output: gSizeSort: ソートされた元のクラスタサイズ も返す

numTmpCluster = size(gMean,1);

gSizeTmp = zeros(1, numTmpCluster);
for k=1:numTmpCluster
    gSizeTmp(k) = sum(assignedlabel==k); % 現在のサイズを計算
end
%gSizeTmp
[gSizeSort, gLabelmap] = sort(gSizeTmp, 'descend'); % サイズの大きいものほど若い番号とする
point2label = zeros(length(assignedlabel),1);
%numNewCluster = numTmpCluster - length(gSmallList);
clustCent = []; % zeros(dim, numNewCluster);
for newClustID=1:numTmpCluster % 新しいクラスタID
    if(gSizeSort(newClustID)==0), break, end % クラスタサイズゼロが出てきたら終わり
    origClustID = gLabelmap(newClustID); % newClustID はもともと gLabelmap(newClustID)番目にあった
    point2label(assignedlabel==origClustID) = newClustID;
    clustCent = [clustCent; gMean(origClustID, :)];
end

end
