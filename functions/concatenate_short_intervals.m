function [ Iset ] = concatenate_short_intervals( Iset, lmin )
%concatenate_short_intervals 短すぎる区間は前に連結(ただし一番前だけは後ろに連結)
%   詳細説明をここに記述

% この実装は一時的（不完全）
% 一回のマージで lmin 以上になることを前提としているが
% lminが大きなときは2回以上のマージが必要になるので対応できていない

% Iset
% lmin
% 一番前だけは後ろに連結
if(Iset(1,2) - Iset(1,1) + 1 < lmin)
    Iset(2,1) = Iset(1,1); % 二番目の区間の開始時刻を１番目の区間のものとする
    Iset(1,:) = [];
end
dur = Iset(:,2) - Iset(:,1) + 1;
%Iset
idx = find(dur<lmin);
idx = sort(idx, 'descend');
%length(idx)

if(length(idx) > 0)
    if(idx(end)==1)
        Nidx = length(idx)-1;
    else
        Nidx = length(idx);
    end
else
    return   % no need to concatenate
end

for i=1:Nidx
%    fprintf('--- check interval %d\n', i);
%     if(i<3), Iset, end
%idx(i)
    Iset(idx(i)-1, 2) = Iset(idx(i), 2); % ひとつ前の区間の終了時刻を今から削除する区間の終了時刻とする
    Iset(idx(i),:) = [];
end

end

