function scatter_clusters( Xsampl, point2index, centroids, clustercolormap, titlestr )
%scatter_clusters サンプル点とクラスタ中心の散布図をカラーで描画
%   Xsampl (N x dim matrix)
%   point2index (N次元 vector) : 各点が属するクラスタID
%   centroids (K x dim matrix) : クラスタ中心
%   clustercolormap (K以上 x 3 matrix) : 各クラスタを何色で塗るか
%   titlestr (string) : 図のタイトル

scatter(Xsampl(:,1), Xsampl(:,2), 10, clustercolormap(point2index,:))
title( titlestr )
hold on
nClust = size(centroids,1);
scatter(centroids(:,1), centroids(:,2), 40, clustercolormap(1:nClust, :), 'filled');
hold off
% IDをtext表示
gcaxlim = get(gca, 'XLim'); 
textoffset = (gcaxlim(2)-gcaxlim(1))/50;
for k=1:size(centroids,1)
    text(centroids(k,1)+textoffset, centroids(k,2), num2str(k), 'FontSize', 12);
end

end

