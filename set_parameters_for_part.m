function param = set_parameters_for_part(dim, seqid, partid)
%set_parameters_for_part
%   系列やパートごとに設定を変えられるように，引数にseqid, partid を取ることにする
%   戻り値は構造体

% --- default parameter values
param.default.LDSorder = 2;
param.default.LDSalgo = 15; % LDS同定のアルゴリズム 1:制約なし回帰, 15:ベイズ推定, 16: ベイズ推定+upper_bound

% --- Parameters for finding scalespace peaks
% 差分(速度)ベクトルノルム系列の傾き極大,極小 (つまり変曲点)をスケールスペースで見つける
param.seg.MinPeakHeight = 0.01 * sqrt(dim); % ピークの絶対値条件
param.seg.MinDiffFromNeighbor = 0.00; % 周辺との差がこの値より大きい場合のみ検出
param.seg.MinScale = 2; % 最小スケール (これを1にすると元の信号での細かなピークが出やすくなる)

% その後，極大は時間的に負の方向に，極小は正の方向に移動させる (移動量の計算はPeakRatioForShiftによる)
param.seg.peakratio_for_shift = 0.8; % 傾きがどのくらい小さくなるところまで点を移動させるか

% --- Parameters for Viterbi Concatenate
% 分割に要するコスト この値が小さいほど分割が増える
if(partid==5) % dim = 3
    param.seg.divCost = 0.01;
else % dim = 2
    param.seg.divCost = 0.02;
end

param.seg.max_interval_cat = 50;
param.seg.LDSorder = param.default.LDSorder;
param.seg.LDSalgo = 1; % 隣接区間併合時には制約なしをもちいる

param.seg.lmin = param.seg.LDSorder + 1; % viterbi連結(LDSの近似がうまくいく範囲で)の前に細かな区間はマージしておく

param.seg.use_origpeak = false; %true; % 現在は第一要素しか使っていないので適宜変えること
param.seg.pkthres = 0.004; %pkthres = 0.002; %## param ## 平らなピークを除外（ピークとその両隣との間には，最小振幅差としてpkthresが必要とする）

%-----------------------------------------------------------
% 距離行列のパラメタ
param.dist.disttype = 1; %12; % 1: 通常のKL;  12: 系列の初期値から生成(ただし12だと通常のKLdivより分離しづらい. ダイナミクスというより収束先での比較になっているようにみえる)
param.dist.manualQdiag = 0.01; % 共分散をこの値 x 単位行列で手動設定する
param.dist.centerize = true;
param.dist.quantile = 0.50;

%-----------------------------------------------------------
%param.clust.scLDS_k = 2; % LDSの spectral clustering の分割数
param.clust.scLDS_k = 16; % LDSの spectral clustering の分割数

end
