clear variables
close all

addpath('./functions')

%% 表情譜プロット
% 'face_seq%02d_part%02d.mat' を読み込んで表情譜を作成
%  あとで顔映像と結合する場合:
%   - gen_video = true とすると，表情譜上で時刻とともに縦棒が移動していく映像が生成される
%   - T を最初 20ぐらいにして試すと，出力映像のイメージが分かる
%   - これを avisynth などで顔特徴点の追跡映像と結合すればよい
%  --- avsファイルの例 (これを .avs という拡張子のテキストファイルとして用意し，virtual dub などのツールにD&D)---
% video = DirectShowSource("顔映像.m4v")
% score = DirectShowSource("facescore_with_bar.m4v")
% StackHorizontal( Crop(video, 320, 0, 640, 720) , score)  # left, top, width, height, bool "align"

%%
gen_video = false;
% 系列全体について生成するときは for t=1:20 (お試し版) を for t=1:T にすること

% 系列読み込み
seqid = 1;
%partidlist = [1 4 2 7];
%partidlist = [1 5];
partidlist = 1;
numparts = length(partidlist);
partnamelist = {'Mouth', 'Eye', 'Eyebrow', 'Head-V'};
%partnamelist = {'Mouth', 'Head-V'};

for p = 1:numparts
    pid = partidlist(p);
    ifname = sprintf('face_seq%02d_part%02d.mat', seqid, pid);
    part{p} = load(ifname);
    part{p}.name = partnamelist(p);
end

%%
%figsize =  [200,200,1280,720];
figsize =  [0,0, 1800, 900];
intervalfig = figure('Name', 'Interval sequences', 'Position', figsize);

Nrows = 1; % 何分割（何行にして表示するか）
Tseg = floor(size(part{1}.X,2)/Nrows); % 一段あたりに表示する時間長
xoffset_for_seqname = -(Tseg/12); % 系列名の表示をどのあたりにするか

ylmunit = [-4, 4];
yoffsetunit = ylmunit(2) - ylmunit(1);
ylm = [ylmunit(1), ylmunit(1) + yoffsetunit * numparts]; % numparts倍する

% 1段目をプロット
axrows(1) = subplot(Nrows, 1, 1);
hold on
ymiddlepart = zeros(1, numparts);
for p = 1:numparts
    pid = partidlist(p);

    yoffset = yoffsetunit*(p-1); % グラフの縦側オフセット

    labeledIset = [part{p}.segIset, part{p}.scLDS_idx];

    plot((part{p}.X + yoffset)');
    %plot((part{p}.X(1,:) + yoffset)');

%    for k=1:size(segIset,1)    % 区間番号を表示
%        text( segIset(k,1), ylm(2), num2str(k), 'FontSize', 6 )
%    end

    ytop = ylmunit(2) + yoffset;
    ybottom = ylmunit(1) + yoffset;
    ymiddle = (ybottom+ytop)/2;
    ymiddlepart(p) = ymiddle;
    text( xoffset_for_seqname, ymiddle, partnamelist{p});
    for k=1:size(labeledIset,1)
        patch([labeledIset(k,1),labeledIset(k,2)+1,labeledIset(k,2)+1,labeledIset(k,1)], [ybottom,ybottom, ytop-1,ytop-1], part{p}.modecolormap(labeledIset(k,3),:));
        h = text( (labeledIset(k,1)+labeledIset(k,2))/2, ymiddle, num2str(labeledIset(k,3)), 'FontSize', 6);
        h.Clipping = 'on';
    end
end
hold off
alpha(0.4)


% 2段目以降表示
for i=2:Nrows
    axrows(i) = subplot(Nrows, 1, i);
    copyobj(allchild(axrows(1)),axrows(i));
end

xlimIset = zeros(Nrows, 3);
ylimIset = zeros(Nrows, 2);
for i=1:Nrows
    xlimIset(i,:) = [Tseg*(i-1), Tseg*i, i];
    set(axrows(i), 'XLim', xlimIset(i,1:2));
    ylimIset(i, :) = get(axrows(i), 'YLim');
end

%% ---- 映像生成 ------------------------------------------------------------------------------
if(gen_video)
xlimIset(1,1) = 1; % 開始が0のはずなので1に変える
T = xlimIset(end,2); % 系列長
time2row = interval2labelseq(xlimIset);
for i=1:Nrows
    axes(axrows(i))
    h(i) = line(ones(1,2)*(xlimIset(i,1)-1), ylimIset(1,:), 'Color', 'b', 'LineWidth', 2);
    h(i).Visible = 'off';

    % テキストも追加 (パッチに重なってしまうので，映像生成のときのみ，2段目以降も追加することにしている)
    % もし映像生成時以外でも表示させたいのならば，if(gen_video)の外に出せばよい
    if(i>1)
        for p=1:numparts
            text( xlimIset(i,1) + xoffset_for_seqname, ymiddlepart(p), partnamelist{p} );
        end
    end
end
%% write avi
vidObj = VideoWriter('facescore_with_bar.m4v', 'MPEG-4');
vidObj.FrameRate = 23.928;
open(vidObj);

h(1).Visible = 'on';
prevrow = 1;
tic
for t=2180:2980  % 少し試す場合
%for t=1:T
    currow = time2row(t);
    if(currow~=prevrow)
        h(prevrow).Visible = 'off';
        h(currow).Visible = 'on';
        prevrow = currow;
    end
    h(currow).XData = [t,t];
    %drawnow
    currFrame = getframe(gcf);
    writeVideo(vidObj, currFrame);
end
toc

close(vidObj);


end
