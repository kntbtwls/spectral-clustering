clear variables
close all

addpath('./functions')

%% 各パートの信号をセグメンテーション & ラベリング
% 0. datadir_root でデータ系列のあるフォルダを指定する
% 1. パラメタ設定の大部分は set_parameters_for_part.m で行う
% 2. 各パートの定義 (顔もしくは頭部の特徴量) は FacialFeature{ partid }.X で与える
% 3. 処理対象の系列IDは seqid で，パートID は partidlist で指定する
%    パートIDは一度に複数指定可能 (for で回す)
% 以上で実行可能

% ラベリング部分を試行錯誤するときは，毎回セグメンテーションしたくない
%  いったん計算しておいた セグメンテーション結果を利用する場合は load_segresult = true とする
%

%%
datadir_root = './data';

load_segresult = true; % trueなら 分節化は読み込むだけ（新たに計算しない）falseなら 分節化から行う
load_idsysresult = true; % LDSパラメタについて、同上

seqid = 1; % 1-5  現在 5系列 (具体的に読み込んでいる系列を見るにはload_sequences の中を見ること)
%partidlist = 5;
partidlist = [1 2 3 4 5 7];     % 用いるパートの指定 (複数パートを一度にfor-loopで処理するときはここを配列にする)

labelmethod = 1;                        % ラベリング方式(0: k-means 1:正負符号)
divide_count = 4;                       % 階層的に分割する際に何段階分割するか

%modecolormap = lines(1000); % LDSモードの色ラベル定義
modecolormap = [ 1, 0.5, 0.5
                 1, 1, 0.5
                 0.5, 1, 0.5
                 0.5, 1, 1
                 0, 0.5, 1
                 1, 0.5, 0.75
                 0.5, 0.5, 0.75
                 0.5, 0, 0.25
                 0.5, 0.25, 0.25
                 1, 0, 0.5
                 0, 0, 0.25
                 0.5, 0.5, 0
                 1, 0.5, 0
                 0.5, 0.5, 0.5
                 0.25, 0, 0.25
                 0, 0, 1];


% --------------- 特徴量データ準備 ---------------------
[landmarkseqSet, headseqSet] = load_sequences(datadir_root); % いったん全部読み込む

landmarkseq = landmarkseqSet{seqid}; % 顔の特徴点
headseq = headseqSet{seqid};
%points_each = permute(landmarkseq, [1 3 2]); % 二番目をx,y,zの指定(それぞれ1,2,3)にしたい場合
%------------------------------
% 顔パート1
FacialFeature{1}.X = [landmarkseq(:,55,1) - landmarkseq(:,49,1),... % 口の両端
        landmarkseq(:,58,2) - landmarkseq(:,52,2)];  % 口中央の上下
%------------------------------
% 顔パート2
FacialFeature{2}.X = [landmarkseq(:,30,2) - landmarkseq(:,18,2),...
    landmarkseq(:,22,2) - landmarkseq(:,18,2)]; % 右眉右端と鼻中心の差(y座標)
%X = landmarkseq(:,18:22, 2; % 右眉
%    landmarkseq(:,23,1) - landmarkseq(:,22,1)]; % 眉間の間隔
%------------------------------
% 顔パート3
FacialFeature{3}.X = [landmarkseq(:,30,2) - landmarkseq(:,27,2),...
    landmarkseq(:,23,2) - landmarkseq(:,27,2)]; % 左眉左端と鼻中心の差(y座標)
%X = landmarkseq(:,18:22, 2; % 左眉
%    landmarkseq(:,23,1) - landmarkseq(:,22,1)]; % 眉間の間隔
%------------------------------
% 顔パート4
%FacialFeature{4}.X = squeeze( landmarkseq(:,37:42,2) );
FacialFeature{4}.X = [landmarkseq(:,42,2) - landmarkseq(:,38,2),...
    landmarkseq(:,41,2) - landmarkseq(:,39,2)];
%[landmarkseq(:,37,2) - landmarkseq(:,20,2)];
%X = landmarkseq(:,37:42,2); % 右目
%------------------------------
% 顔パート5
%FacialFeature{5}.X = squeeze( landmarkseq(:,37:42,2) );
FacialFeature{5}.X = [landmarkseq(:,47,2) - landmarkseq(:,45,2),...
    landmarkseq(:,48,2) - landmarkseq(:,44,2)];
%[landmarkseq(:,37,2) - landmarkseq(:,20,2)];
%X = landmarkseq(:,37:42,2); % 左目
%------------------------------
% 顔ポーズ1
FacialFeature{6}.X = headseq(:,1:3); % orientation [pitch,yaw,roll,scale,tx,ty]
% 顔ポーズ2
%FacialFeature{5}.X = headseq(:,4); % z location (scale)
FacialFeature{7}.X = headseq(:,[1,4,6]); % pitch, scale(z), ty
% 顔ポーズ3
%FacialFeature{6}.X = headseq(:,5:6); % (x,y) localtion
FacialFeature{8}.X = headseq(:,[2,3,5]); % yaw,roll,(x,y) localtion


% ----------------------------------------------------
for partid= partidlist % 各パーツの信号について

    X = FacialFeature{partid}.X;
    %% -------------------- 系列の前処理 ---------------------------
    figure('Name', 'Input signal and smoothed signal');
    subplot(311),plot(X), size(X), title('original input data')

    smoothing_windowsize = 5; % 元信号に対する平滑化
    for col=1:size(X,2)
        X(:,col) = smooth(X(:,col), smoothing_windowsize, 'moving'); % 平滑化いれることにする
    end
    X = X';
    dim = size(X,1); fprintf('dim = %d\n', dim);
    subplot(312),plot(X'), size(X), title('after smoothing')

    Xorig = X;
    X = standardize_sequence(X);    % 標準化
    subplot(313),plot(X'),title('after standardization')

    %% ----------------- 各種パラメタ設定 -------------------
    param = set_parameters_for_part(dim, seqid, partid);

    %% ----------------- セグメンテーション候補点検出 --------------------------------------
    Isetfilename = sprintf('scalespace_viterbi_Iset_seq%d_part%d.mat', seqid, partid);

    if(load_segresult)
        fprintf('input file: %s\n', Isetfilename);
        load(Isetfilename);
    else
        segIset = scalespace_segmentation(X, param.seg);
        %segIset = csegmentation(X, param.seg);

        save(Isetfilename, 'segIset'); % 分節化結果を保存
    end


    %% -------------- 各区間の特徴量/LDSのモデルパラメータを計算 (これを基に初期ラベル付与?)
    % ここの大部分 (Ifeaturesなど) は現在は用いておらず，identify_system と，それによって
    % 計算された cl (LDSのパラメタが入ったセル配列)のみを利用している
    %
    % もし別途初期ラベリングを行うために，各特徴量にアクセスするなら Ifeatures を利用できる
    % Ifeatures のk行目が，区間Ik の特徴ベクトルとなる．また，特定の特徴量のみ取り出すには
    %       fvec = Ifeatures(:,fts(fid('vel')).Mask);
    % とすれば，fvec に 区間内の平均速度ベクトルが取り出される
    %  - 特徴量のIDは fid('vel') などのように連想配列で取り出せる
    %  - Ifeatures の何列目がどの特徴量かは，fts(特徴量ID).Mask 管理している
    %  - 各特徴量は1次元とは限らないので，各特徴量の次元を fts(特徴量ID).Dim で管理している
    %  - 複数の特徴を取り出すには，fts(fid('vel')).Mask | fts(fid('pos')).Mask などと指定できる
    ldsparam_filename = sprintf('ldsparam_seq%d_part%d.mat', seqid, partid);

    if(load_idsysresult)
        fprintf('input file: %s\n', ldsparam_filename);
        load(ldsparam_filename);
    else
        numIntervals = size(segIset,1);
        % 連想配列準備 ('ini' -> 5 とアクセスできるように)
        % ini:区間の始点, fin: 区間の終点, dur:区間長, pos:区間の平均特徴ベクトル, vel:区間の平均差分ベクトル
        flabel = {'k', 'sqerr', 'A', 'b', 'ini', 'fin', 'dur', 'pos', 'vel'};
        fdim = {1, 1, dim*dim*param.default.LDSorder, dim, dim, dim, 1, dim, dim}; % 各特徴量の次元
        fnmode = {0, 0, 0, 0, 3, 3, 3, 3, 5}; % k-meansのK
        numFeatures = length(flabel);

        fid = containers.Map(flabel, num2cell(1:length(flabel))); % fid('特徴量名') -> id
        fts = struct('Name', flabel, 'Dim', fdim, 'Nmode', fnmode); % fts(id).Dim など
        Dim_total = sum([fts(:).Dim]);
        tmpelm = 0;
        for f=1:numFeatures % 各特徴を取り出すためのマスク作成
            fts(f).Mask = false(1, Dim_total);
            fts(f).Mask(tmpelm+1:tmpelm+fts(f).Dim) = true;
            tmpelm = tmpelm + fts(f).Dim;
        end

        %
        dsignal = X(:,2:end) - X(:,1:end-1);
        Ifeatures = zeros(Dim_total, numIntervals); % 全特徴を並べた行列
                                                    %
        Xtmp{1} = X;
        cl = cell(1,numIntervals);
        for k=1:numIntervals
            Isettmp{1} = segIset(k,:);

            [cl{k}.A, cl{k}.b, cl{k}.Q, cl{k}.xinit, tmpalpha, tmpbeta, tmpgamma, Acov, Acovinv, Err] = identify_system(Xtmp, Isettmp, param.default.LDSalgo, true, true, 1, param.default.LDSorder);
            sqerr = norm(Err, 'fro')^2;

            cl{k}.Iset{1} = Isettmp{1};

            Ifeatures(:, k) = [k; sqerr; cl{k}.A(:); cl{k}.b;...
                               X(:,segIset(k,1)); X(:,segIset(k,2)); segIset(k,2)-segIset(k,1)+1;...
                               mean(X(:,segIset(k,1):segIset(k,2)), 2); mean(dsignal(:,segIset(k,1):segIset(k,2)), 2)];
        end

        Ifeatures = Ifeatures';

        save(ldsparam_filename, 'cl');
    end

    %% 各区間から求めたLDS間の距離行列
    [Dist, distvec] = calc_distancematrix(X, cl, param.dist);

    % 実際には距離の二乗がかかっているので分散に対応
    % 全体の分布をみて外れ値除去
    sigma2 = quantile(distvec, param.dist.quantile);
    Wgraph = exp(-Dist/sigma2);

    if(labelmethod)
        scLDS_idx = zeros(size(Wgraph, 1), 1);

        divc = divide_count;
        while(divc > 0)
            scLDS_idx = spectral_clustering(Wgraph, Dist, scLDS_idx);
            divc = divc-1;
        end
        % ラベル構造を保存しておく
        % ツリー構造のものとリラベル後のものと両方
        idxfilename = sprintf('labellist_seq%d_part%d_tree.mat', seqid, partid);
        save(idxfilename, 'scLDS_idx');

        scLDS_idx = relabel_by_idx(scLDS_idx);
        idxfilename = sprintf('labellist_seq%d_part%d.mat', seqid, partid);
        save(idxfilename, 'scLDS_idx');
    else
        Dgraph = diag(sum(Wgraph,2));
        %Lgraph = eye(size(Dist,1)) - (Dgraph^-1)*Wgraph; % normalization rw
        Lgraph = eye(size(Dist,1)) - (Dgraph^-0.5)*Wgraph*(Dgraph^-0.5); %normalization sym
        [EVECgraph, EVALgraph] = eig(Lgraph);
        [scLDS_idx, scLDS_cent] = kmeans(EVECgraph(:, 2:param.clust.scLDS_k), param.clust.scLDS_k);
        [scLDS_idx, scLDS_cent] = relabel_by_size(scLDS_idx, scLDS_cent); % クラスタサイズ順にラベルを振りなおす

        figure('Name', 'SC', 'Position', [100,100,600,600]);
        if(param.clust.scLDS_k>2)
            % Spectral Clustering の2,3番目に小さい固有値に対応する固有ベクトルを2次元でプロット
            % 3次元以上ある場合は，クラスタ同士が重なってくるため，あくまで参考程度
            scatter_clusters(EVECgraph(:,2:3), scLDS_idx, scLDS_cent, modecolormap, 'Spectral Clustering');
        else
            scatter(scLDS_idx, EVECgraph(:,2));
            xlim([0.9,2.1])
            xlabel('Cluster ID');
            ylabel('Eig.value');
        end
        drawnow
    end

    %% ===================== 区間のラベルを信号とともに可視化 ==========================================
    testfig = figure('Name', 'Segmentation/Labeling result');

    Nrows = 4; % 何分割（何行にして表示するか）
    Tseg = floor(size(X,2)/Nrows); % 一段あたりに表示する時間長
    xoffset_for_seqname = -(Tseg/10); % 系列名の表示をどのあたりにするか

    axrows(1) = subplot(Nrows, 1, 1);
    plot_with_divpoints(segIset, X');
    ylm = get(gca, 'Ylim');
    % 区間番号を表示
    for k=1:size(segIset,1)
        h = text( segIset(k,1), ylm(2), num2str(k), 'FontSize', 6 );
        h.Clipping = 'on';
    end

    % patchでラベル系列を可視化
    baseclr = 0;
    ybeg = ylm(1);

    % クラスタリング後のモードラベルをカラー表示
    text( xoffset_for_seqname, ybeg-0.5, 'Spec.Clst')
    for k=1:size(segIset,1)
        patch([segIset(k,1),segIset(k,2)+1,segIset(k,2)+1,segIset(k,1)], [ybeg-0.9,ybeg-0.9,ybeg,ybeg], modecolormap(scLDS_idx(k),:));
        h = text( (segIset(k,1)+segIset(k,2))/2, ybeg-0.5, num2str(scLDS_idx(k)), 'FontSize', 6);     h.Clipping = 'on';
    end
    alpha(0.4)

    % 2段目以降表示
    for i=2:Nrows
        axrows(i) = subplot(Nrows, 1, i);
        copyobj(allchild(axrows(1)),axrows(i));
    end
    for i=1:Nrows
        set(axrows(i), 'XLim', [Tseg*(i-1),Tseg*i]);
    end

    %% 保存
    ofname = sprintf('face_seq%02d_part%02d.mat', seqid, partid);
    save( ofname, 'X', 'segIset', 'Dist', 'distvec', 'modecolormap','scLDS_idx',  'param');

end %partid
