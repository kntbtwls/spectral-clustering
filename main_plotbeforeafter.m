clear variables
close all

%%% 気付き前後比較
% 気付きタイミングの前後でラベルがどのように変化しているかをプロット

%% 準備
addpath('./functions')
% 調べたい系列とパーツを指定
% 気付き発生区間の番号も用意
seqid = 1;
partidlist = [1 3 2 5];
numparts = length(partidlist);
partnamelist = {'Mouth', 'Eye', 'Eyebrow', 'Head-V'};
notification_int_list = [45 227 186 32];    % uekiQ1
% $$$ notification_int_list = [52 217 241 17];    % uekiQ2
% $$$ notification_int_list = [71 177 197 47];    % taharaQ2
% $$$ notification_int_list = [105 226 194 85];   %taharaQ4
% $$$ notification_int_list = [65 249 280 18];    %yanoQ4

%% パーツ毎処理
for p = 1:numparts
    % ラベル系列読み込み
    pid = partidlist(p);
    labellistname = sprintf('labellist_seq%d_part%d.mat', seqid, pid);
    labellist{p} = load(labellistname);
    labellist{p}.name = partnamelist(p);

    figurename = sprintf('Label freq.: %s', partnamelist{p});
    figure('Name', figurename)
    subplot(1,2,1)
    histogram(labellist{p}.scLDS_idx(1:notification_int_list(p)-1,1),16)
    xlim([1 16])
    title('before')
    subplot(1,2,2)
    histogram(labellist{p}.scLDS_idx(notification_int_list(p):end,1),16)
    xlim([1 16])
    title('after')

    ldsparamname = sprintf('ldsparam_seq%d_part%d.mat', seqid, pid);
    ldsparam{p} = load(ldsparamname);
    ldsparam{p}.name = partnamelist{p};
    llengthbefore{p}=zeros(16,1);
    llengthafter{p}=zeros(16,1);

    % 区間長頻度用の行列を用意しとく
    for i = 1:16
        llengthfreqbefore{p}{i} = [];
        llengthfreqafter{p}{i} = [];
    end

    for i = 1:notification_int_list(p)-1
        llengthbefore{p}(labellist{p}.scLDS_idx(i,1),1) = ...
            llengthbefore{p}(labellist{p}.scLDS_idx(i,1),1) + ...
            ldsparam{p}.cl{i}.Iset{1}(1,2) - ldsparam{p}.cl{i}.Iset{1}(1,1) + 1;
        llengthfreqbefore{p}{labellist{p}.scLDS_idx(i,1)} = [llengthfreqbefore{p}{labellist{p}.scLDS_idx(i,1)}; ldsparam{p}.cl{i}.Iset{1}(1,2) - ldsparam{p}.cl{i}.Iset{1}(1,1) + 1;];

    end
    for i = notification_int_list(p):size(labellist{p}.scLDS_idx,1)
        llengthafter{p}(labellist{p}.scLDS_idx(i,1),1) = ...
            llengthafter{p}(labellist{p}.scLDS_idx(i,1),1) + ...
            ldsparam{p}.cl{i}.Iset{1}(1,2) - ldsparam{p}.cl{i}.Iset{1}(1,1) + 1;
        llengthfreqafter{p}{labellist{p}.scLDS_idx(i,1)} = [llengthfreqafter{p}{labellist{p}.scLDS_idx(i,1)}; ldsparam{p}.cl{i}.Iset{1}(1,2) - ldsparam{p}.cl{i}.Iset{1}(1,1) + 1;];
    end
    figurename = sprintf('Label length: %s', partnamelist{p});
    figure('Name', figurename)
    subplot(1,2,1)
    bar(llengthbefore{p})
    xlim([0 16])
    title('before')
    subplot(1,2,2)
    bar(llengthafter{p})
    xlim([0 16])
    title('after')

    figurename = sprintf('length freq.: %s', partnamelist{p});
    figure('Name', figurename)
    subplot(1,2,1)
    histogram(llengthbefore{p})
    xlim([1 16])
    title('before')
    subplot(1,2,2)
    histogram(labellist{p}.scLDS_idx(notification_int_list(p):end,1),16)
    xlim([1 16])
    title('after')
end
